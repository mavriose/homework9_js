const test = document.getElementsByClassName('tabs-title');
for (let i = 0; i<test.length;i++){
    test[i].addEventListener('click',switchTabs)
}
function switchTabs() {
    const elemUl = this.parentElement;
    const allLi = elemUl.children;
    const liArr = Array.prototype.slice.call(allLi);
    liArr.forEach(function (item) {
        item.classList.remove('activeLi')
    });
    this.classList.add('activeLi')
    const tabsContent = document.querySelectorAll('.tabs-content');
    for(let i = 0;i < tabsContent.length;i++){
        tabsContent[i].classList.remove('active')
    }
    const pos = liArr.indexOf(this);
    tabsContent[pos].classList.add('active')
}